package com.example.basicandroid.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.basicandroid.*
import com.example.basicandroid.contracts.MainContract
import com.example.basicandroid.models.FdaReponseResult
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MainContract.View,
    AppCompatActivity() {

    private var presenter: MainContract.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataSource = MainRepository()
        presenter = MainPresenter(dataSource)
        presenter?.initView(this)
        presenter?.requestVaccines()
    }

    override fun receiveVaccines(vaccines: List<FdaReponseResult>) {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerview_vaccines.layoutManager = linearLayoutManager

        val vaccinesAdapter = VaccinesAdapter(vaccines)
        recyclerview_vaccines.adapter = vaccinesAdapter
    }

}