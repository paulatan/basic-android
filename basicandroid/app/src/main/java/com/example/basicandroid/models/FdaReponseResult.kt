package com.example.basicandroid.models

import com.google.gson.annotations.SerializedName

data class FdaReponseResult(
    @SerializedName("application_number") val applicationNumber: String,
    @SerializedName("sponsor_name") val sponsorName: String)
