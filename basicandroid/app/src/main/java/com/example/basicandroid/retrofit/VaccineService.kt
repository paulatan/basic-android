package com.example.basicandroid.retrofit

import com.example.basicandroid.models.FdaResponse
import retrofit2.Call
import retrofit2.http.GET

interface VaccineService {

    @GET("drug/drugsfda.json?limit=10")
    fun getVaccines(): Call<FdaResponse>

}