package com.example.basicandroid.second

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.basicandroid.R
import kotlinx.android.synthetic.main.activity_main.*

class SecondActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.design_default_color_primary_dark)
        )
        Log.d("lifecycle", "SecondActivity onCreate")
    }

    override fun onStart() {
        super.onStart()
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.design_default_color_error)
        )
        Log.d("lifecycle", "SecondActivity onStart")
    }

    override fun onResume() {
        super.onResume()
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.design_default_color_secondary_variant)
        )
        Log.d("lifecycle", "SecondActivity onResume")
    }

    override fun onPause() {
        super.onPause()
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.purple_200)
        )
        Log.d("lifecycle", "SecondActivity onPause")
    }

    override fun onStop() {
        super.onStop()
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.teal_200)
        )
        Log.d("lifecycle", "SecondActivity onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        constraintlayout_main.setBackgroundColor(
            getColor(R.color.material_on_background_disabled)
        )
        Log.d("lifecycle", "SecondActivity onDestroy")
    }
}