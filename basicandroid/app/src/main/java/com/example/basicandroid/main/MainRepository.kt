package com.example.basicandroid.main

import android.util.Log
import com.example.basicandroid.models.FdaReponseResult
import com.example.basicandroid.models.FdaResponse
import com.example.basicandroid.datasources.MainDataSource
import com.example.basicandroid.retrofit.VaccineService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainRepository: MainDataSource {

    override fun getVaccines(listener: Listener) {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.fda.gov/")
            .build()

        val vaccineService = retrofit.create(VaccineService::class.java)

        vaccineService.getVaccines().enqueue(object : Callback<FdaResponse> {
            override fun onResponse(
                call: Call<FdaResponse>,
                response: Response<FdaResponse>
            ) {
                //initVaccines(response.body()?.results!!)
                listener.onGetVaccinesSuccess(response.body()?.results!!)
            }

            override fun onFailure(call: Call<FdaResponse>, t: Throwable) {
                Log.d("onFailure", "${t.message}")
            }
        })
    }

    interface Listener {

        fun onGetVaccinesSuccess(vaccines: List<FdaReponseResult>)

        fun onGetVaccinesError()

    }
}