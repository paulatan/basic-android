package com.example.basicandroid.models

data class FdaResponse(
    val results: List<FdaReponseResult>)
