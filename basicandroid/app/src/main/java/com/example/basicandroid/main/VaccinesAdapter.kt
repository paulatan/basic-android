package com.example.basicandroid.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.basicandroid.R
import com.example.basicandroid.models.FdaReponseResult
import kotlinx.android.synthetic.main.item_vaccine.view.*

class VaccinesAdapter(val vaccines: List<FdaReponseResult>):
    RecyclerView.Adapter<VaccinesAdapter.VaccineViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VaccineViewHolder {
        val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_vaccine, parent, false)
        return VaccineViewHolder(view)
    }

    override fun onBindViewHolder(holder: VaccineViewHolder, position: Int) {
        holder.bind(vaccines[position])
    }

    override fun getItemCount(): Int {
        return vaccines.size
    }

    inner class VaccineViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(vaccine: FdaReponseResult) {
            itemView.item_vaccine_application_number.text =
                vaccine.applicationNumber
            itemView.item_vaccine_sponsor_name.text =
                vaccine.sponsorName
        }

    }

}