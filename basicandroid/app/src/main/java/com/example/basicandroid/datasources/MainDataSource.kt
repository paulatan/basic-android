package com.example.basicandroid.datasources

import com.example.basicandroid.main.MainRepository

interface MainDataSource {

    fun getVaccines(listener: MainRepository.Listener)

}