package com.example.basicandroid.main

import com.example.basicandroid.models.FdaReponseResult
import com.example.basicandroid.datasources.MainDataSource
import com.example.basicandroid.contracts.MainContract

class MainPresenter(val dataSource: MainDataSource):
    MainContract.Presenter {

    private var view: MainContract.View? = null

    private var listener = object: MainRepository.Listener {
        override fun onGetVaccinesSuccess(vaccines: List<FdaReponseResult>) {
            view?.receiveVaccines(vaccines)
        }

        override fun onGetVaccinesError() {}
    }

    override fun initView(view: MainContract.View) {
        this.view = view
    }

    override fun requestVaccines() {
        dataSource.getVaccines(listener)
    }
}