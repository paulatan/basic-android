package com.example.basicandroid.contracts

import com.example.basicandroid.models.FdaReponseResult

interface MainContract {

    interface View {

        fun receiveVaccines(vaccines: List<FdaReponseResult>)

    }

    interface Presenter {

        fun initView(view: View)

        fun requestVaccines()

    }

}