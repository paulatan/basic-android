# Basic Android Development

## [Session 1](Session1.md)
1. Android Studio Basics
- [Creating an Android Project](https://developer.android.com/training/basics/firstapp/creating-project)
2. [Running the App](https://developer.android.com/training/basics/firstapp/running-app)
3. [Activity Lifecycle](https://developer.android.com/guide/components/activities/activity-lifecycle)
- Coding: Empty project, lifecycle demo

## Session 2
1. Simple User Interface (UI)
- TextBox, Button, ImageView
- [Strings.xml](https://developer.android.com/guide/topics/resources/string-resource)
- [Layouts](https://developer.android.com/guide/topics/ui/declaring-layout#CommonLayouts)
- Coding: Creating a simple login screen
2. Create a [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview)
- Coding: Create a recyclerview from a static list of vaccines (String)

## Session 3

1. API Calls through [Retrofit](https://square.github.io/retrofit/)
- Intro to data classes
- Import [Gson](https://github.com/google/gson) as JSON parser
- API to use: https://api.fda.gov/drug/drugsfda.json?limit=10
- Coding: Fetching of vaccines list, display in a RecyclerView
2. [Debugging](https://developer.android.com/studio/debug#startdebug)

## Session 4

1. MVP (Model-View-Presenter) Architecture
- [Reference 1 (Easy)](https://medium.com/mindorks/android-mvp-pattern-debunked-for-beginners-kotlin-c56c888222e0)
- [Reference 2 (Expanded)](https://android.jlelse.eu/trying-to-build-android-mvp-app-in-kotlin-afdff9da2f28)
- Coding: Create View-> Presenter-> Model flow 
2. Folder Structure Organization

## Additional: 
- Kotlin Null Safety
- Common Keyboard Shortcuts (along the way)
- Saving data locally through [SharedPreferences](https://developer.android.com/training/data-storage/shared-preferences)

## Advanced:
- Push Notifications