# Session 1 
##### [(slides here)](https://docs.google.com/presentation/d/1ma2U-3I21mZqNAt0-zTVZqASWQqbL6AsbmCFajWULDk/edit#slide=id.gc6f73a04f_0_0)

## 1. Android Studio Basics
- [Creating an Android Project](https://developer.android.com/training/basics/firstapp/creating-project)
## 2. [Running the App](https://developer.android.com/training/basics/firstapp/running-app)
## 3. [Activity Lifecycle](https://developer.android.com/guide/components/activities/activity-lifecycle)
- Coding: Empty project, lifecycle demo

### Activity core set of callbacks
- onCreate()
- onStart()
- onResume()
- onPause()
- onStop()
- onDestroy()

![The activity lifecycle](https://developer.android.com/guide/components/images/activity_lifecycle.png)

#### onCreate\(\)
- Called when the activity is first created by the system
- Do application startup logics, initial setups here
- Do logics that must happen only ONCE in a screen’s lifecycle
- Declare the user interface 
`setContentView`(\<activity layout resource id\>) here

#### onStart\(\)
- Called when the activity is visible to the user
- Activity will enter foreground and can accept interactions

#### onResume\(\)
- Called immediately after onStart\(\)
- App interacts with the user
- Stays in this state will app is in focus
- Call operations that needs UI update/refresh - when app is brought to foreground

#### onPause\(\)
- Called when user is leaving the activity
- Called when an interruptive event occurs while onResume() state
- Receiving a phone call, opening another screen, navigating to another app, device screen turned off
- Activity is no longer in foreground
- Very brief, not intended for network/database operations

#### onStop\(\)
- Called when activity is entirely no longer visible
- Release resources not needed when app is not visible (pause animations) 

#### onDestroy\(\)
- Called when activity is finished running, before it is destroyed
- Can also be invoked by calling finish()


##### Example: Activity A starts Activity B
```
1. Activity A onPause()
2. Activity B onCreate(), onStart(), onResume()
3. Activity B is completely on focus
4. Activity A onPause()
```

##### User presses Back button
```
Activity 
1. onPause()
2. onStop()
3. onDestroy()
```

### Advanced Reading
- [`onSaveInstanceState()`](https://developer.android.com/guide/components/activities/activity-lifecycle#save-simple,-lightweight-ui-state-using-onsaveinstancestate)

### Demo Code \<link to demo\>







