# Session 4
##### [(slides here)](https://docs.google.com/presentation/d/1ma2U-3I21mZqNAt0-zTVZqASWQqbL6AsbmCFajWULDk/edit#slide=id.gc6f73a04f_0_0)

## 1. MVP (Model-View-Presenter) Architecture

![MVP Architecture](https://www.vogella.com/tutorials/AndroidArchitecture/img/xmvp_overview.png.pagespeed.ic.Yl95B-nieq.png)

1. Create Contract Interface between View and Presenter
2. Create interface for Model
3. Implement methods in View and Presenter from Contract
4. In View: Add Presenter property and create instance
5. In Presenter: Receive Model property
6. Bind View to the Presenter

## 2. Folder Structure Organization

Create separate packages for
- each screen (activity)
- contracts
- datasources
- models
- (optional): retrofit