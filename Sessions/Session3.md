# Session 3 
##### [(slides here)](https://docs.google.com/presentation/d/1ma2U-3I21mZqNAt0-zTVZqASWQqbL6AsbmCFajWULDk/edit#slide=id.gc6f73a04f_0_0)

## 1. API Calls through [Retrofit](https://square.github.io/retrofit/)
1. Add dependencies
```properties
//Retrofit
implementation 'com.squareup.retrofit2:retrofit:2.9.0'

//Gson
implementation 'com.google.code.gson:gson:2.8.6'
implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
```
3. Create a service interface: 
```kotlin
interface VaccinesService() {

	@GET("drug/drugsfda.json")
	fun getVaccines(): Call<FdaResponse>

}
```

3. Create a Retrofit instance and implementation of service interface:
```kotlin
// In MainActivity.kt
val retrofit = Retrofit.Builder()
	.baseUrl("https://api.fda.gov/")
	.addConverterFactory(GsonConverterFactory.create())
	.build()
	
val service = retrofit.create(VaccinesService::class.java)
```

4. Create HTTP request
```kotlin
service.getVaccines().enqueue(object: Callback<FdaResponse>) {

}
```
## 2. [Debugging](https://developer.android.com/studio/debug#startdebug)
- Using [breakpoints](https://developer.android.com/studio/debug#breakPoints)

### Additional concepts
1. [Data classes](https://kotlinlang.org/docs/reference/data-classes.html#data-classes) - classes with the purpose of holding data
2. Kotlin [Nullables](https://kotlinlang.org/docs/reference/null-safety.html) - references that can either have a value or be `null`