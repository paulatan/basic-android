# Session 2
##### [(slides here)](https://docs.google.com/presentation/d/1ma2U-3I21mZqNAt0-zTVZqASWQqbL6AsbmCFajWULDk/edit#slide=id.gc6f73a04f_0_0)

## 1. Simple User Interface (UI)

## 2. Create a [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview)

What is a RecyclerView?
- Display rows of data
- Optimized memory usage
- Reuses the view for new items
- Only loads in memory what can fit on the screen

Pre-requisites
1. XML Layout for item in list
2. List of objects 


In activity, 
- Add layout manager to RecyclerView
- Assign RecyclerView.Adapter (custom class) to RecyclerView 

```kotlin
// 1. Add layout manager to RecyclerView
val layoutManager = LinearLayoutManager(this)
layoutManager.orientation = LinearLayoutManager.VERTICAL
recyclerview_vaccines.layoutManager = layoutManager


// 2. Assign custom RecyclerView.Adapter to RecyclerView
val adapter = VaccinesAdapter(this, vaccinesList)
recyclerview_vaccines.adapter = adapter
```

In adapter [(sample code)](https://gist.github.com/pvtan/3efa73085bdc6ef2029b713bd51e4abc), 
- Override RecyclerView.Adapter methods
- Connect .xml layout to custom RecyclerView.Adapter




